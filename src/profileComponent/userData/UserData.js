import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,Image,Animated,Easing,
} from 'react-native'

import userDataStyle from '../userData/userDataStyle'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import Strings from '../../../utils/Strings'

function UserData (props){

  const position = new Animated.ValueXY(0,0)
  Animated.spring(position,{

    toValue:{x:10,y:10},
    duration:1000,
    easing:Easing.bounce
  }).start()
 
    return (
      <Animated.View style={[userDataStyle.CardView]}>
      <Image source={require('../../../assests/Images/userProfile.png')} style= {userDataStyle.userProfile} />

    <View style={{padding:wp('5%')}}>

    <Text style={userDataStyle.userNameText}> Dino Matthew</Text>
    <Text style={userDataStyle.userDetailText}>dinomatt@yennerhosp.com</Text>

    <View style={{flexDirection : 'row',justifyContent:'space-between'}}> 

    <Text style={userDataStyle.userDetailText}>9025849334</Text> 
    <Image source={require('../../../assests/Images/countryIcon.png')} style= {userDataStyle.countryImage} />

    </View>
  
    {/* <View style={{flexDirection : 'row',justifyContent:'space-between'}}>  */}
      <Text style={userDataStyle.hospitalNameText}> Jenner Hospital</Text>
      {/* <Image source={require('../../assests/Images/countryIcon.png')} style= {userDataStyle.countryImage} /> */}

      {/* </View> */}
      <View style={userDataStyle.ViewLine}></View> 

      <View style={{flexDirection : 'row',justifyContent:'space-between',padding:wp('5%')}}> 
      <View style={userDataStyle.dropDownView}> 
    <Image source={require('../../../assests/Images/menu.png')} style={userDataStyle.otherIcon}/>
     
     <Text > H-1B +2</Text>
     <Image source={require('../../../assests/Images/menu.png')}  style={userDataStyle.otherIcon}/>

     </View>

     <View style={userDataStyle.dropDownView}> 
     <Image source={require('../../../assests/Images/menu.png')}  style={userDataStyle.otherIcon}/>

     <Text>32423423423</Text>

     </View>

   </View>

    </View>
    </Animated.View>
    )};
  export default UserData 