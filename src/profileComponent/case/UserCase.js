import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,Image
} from 'react-native'

import styles from '../case/styles'
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
function UserCase (props){
  
 const colorView = props.viewColor
    return (
      <View>
      <View style={[styles.CardView,{ backgroundColor: colorView }]}>
    <Text style={styles.caseText}>{props.caseText}</Text>
    <Text style={styles.numberText}>{props.textData}</Text>
    </View>
    </View>
    )};
  export default UserCase 