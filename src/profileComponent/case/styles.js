import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  CardView: {
    height: wp('35%'),
    width: wp('30%'),
    
    shadowColor: Colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    padding:wp('5%'),
     alignContent:'center',
     justifyContent:'flex-start'
  },
  ViewLine:{
  height:1,
  backgroundColor:Colors.greyButton_color,
  marginTop:7,
  
  },


 caseText:{

 color:Colors.white_color,
 alignSelf:'center',
 fontSize:wp('3.5%'),
 margin:10,
 fontWeight:'400'
 

 },
 numberText:{

  color:Colors.white_color,
  alignSelf:'center',
  fontSize:wp('8%'),
  margin:10
 
  },
 
 
  
});
export default styles;
