import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity, Text, View,FlatList, Image} from 'react-native';

import styles from '../../../src/profileComponent/basicInfo/styles';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Strings from '../../../utils/Strings';
import * as Progress from 'react-native-progress';

function BasicInformation(props) {
  //*************** book flatList data **************
  const data = props.data
  


  _onPressButton=()=>
  {

   props.onClick()

  }



renderItem = ({item}) => (
  <View >
    
    <Image source={require('../../../assests/Images/userProfile.png') 
 } style= {styles.userProfile} />
 
 
   </View>
 
 );
     //*******************end list */
 
  
  
  return (
    <View style={styles.mainView}>
      <Text style={styles.basicInfoText}>{Strings.basicInformation} </Text>
      <View style={styles.basicInfoView}>
        <View style={styles.CardView}>
          <Text style={styles.userProfileText}>{Strings.profile}</Text>
          <Text style={styles.userDetailText}>{Strings.completionStatus}</Text>
          <Text style={styles.completeProfilePercentage}>60%</Text>
          <Progress.Bar progress={0.6} width={wp('30%')} style={{marginTop:wp('2%')}}/>

          <Text style={styles.userDetailText}>{Strings.modifiedOn}</Text>
          <Text style={styles.infoData}>jun 04,2019</Text>

        

    </View>

        <View style={styles.CardView}>
          <Text style={styles.userProfileText}>{Strings.relative}</Text>
          <Text style={styles.userDetailText}>{Strings.totalRelative}</Text>
  <Text style={styles.completeProfilePercentage}>{props.userCount}</Text>



          <View style={{ width:wp('50%'),height:wp('10%') }}>
       <FlatList
                style={{width: wp('50%')}}
                data={data}
                renderItem={renderItem}
                horizontal={true}
                
                />
  
        </View>
          <TouchableOpacity onPress={_onPressButton}>
            <Text style={styles.addRelative}>{Strings.addRelatives}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}
export default BasicInformation;
