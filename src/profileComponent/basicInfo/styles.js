import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  CardView: {
    height: wp('57%'),
    width: wp('40%'),
    backgroundColor: Colors.white_color,
    shadowColor: Colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    padding:wp('3%'),
     alignContent:'center',
     justifyContent:'flex-start'
  },
  ViewLine:{
  height:1,
  backgroundColor:Colors.greyButton_color,
  marginTop:7,
  
  },

 mainView:{

 height:wp('65%'),
 
 marginTop:wp('25%'),

 
 },

 userProfileText:{
 

    fontWeight:'600',
    fontSize:wp('5%')

 },


 basicInfoView:
 {
    flexDirection:'row',justifyContent:'space-around',marginTop:wp('5%')
 },

 basicInfoText:{

    fontWeight:'700',
    fontSize:wp('5%'),
    marginLeft:wp('3%')

 },

 userProfile: {
   height: wp('7%'),
   width: wp('8%'),
   alignSelf:'center',
   borderRadius:wp('10%/2'),
   marginLeft:wp('-2%')

   
 },
  
  userDetailText:{
    padding:wp('1%'),
    color:Colors.greyButton_color,
    marginTop:wp('1.5%')
 
   },

   infoData:{
    padding:wp('1%'),
    fontSize:wp('3%'),
    fontWeight:'600'
  

   },

   completeProfilePercentage:{
    padding:wp('1%'),
    fontWeight:'600',
    fontSize:wp('7%')
 
   },
   addRelative:{
     
    color:Colors.addRelativeBtnColor,
    fontSize:wp('4%'),
    fontWeight:'600',
    marginTop:wp('2%')
    
 
   },

  
});
export default styles;
