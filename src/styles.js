
import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
// import COLORS from "../../../../utils/Colors";
const window = Dimensions.get('window');


const styles = StyleSheet.create({
    MainContainer: {
      flex: 1,
     
    },
   
    basicInfoText:{
      padding:wp('5%'),
      fontWeight:'700',
      fontSize:wp('5%'),
      marginTop:wp('3%')
  
   },
  
    backgroundImage: {
      
      width: wp('100%'),
      height: wp('60%'),
      }
    });
 export default styles;