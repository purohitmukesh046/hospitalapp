import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  CardView: {
    height: wp('50%'),
    width: wp('90%'),
    backgroundColor: Colors.white_color,
    shadowColor: Colors.shadowColor,
    position:'absolute',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    alignSelf: 'center',
    marginTop:hp('35%'),
    
    elevation: 5,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.borderColor,
    
  },
  ViewLine:{
  height:1,
  backgroundColor:'gray',
  marginTop:7

  },

  otherIcon: {
    height: wp('5%'),
    width: wp('5%'),
  },

  userProfile: {
    height: wp('20%'),
    width: wp('20%'),
    position:'absolute',
    alignSelf:'flex-end',
    marginTop:wp('-10%'),
    borderRadius:wp('20%/2'),
    
    
   

  },
  userDetailText:{
    padding:wp('1.5%'),
    color:Colors.greyButton_color
    
 
   },
   userNameText:{
   fontWeight:'600',
    fontSize:wp('5%'),
    color:Colors.greyButton_color
   },
   hospitalNameText:{

    color:Colors.greyButton_color,
    marginBottom:4
   },

   dropDownView:
   {
    flexDirection : 'row',width:wp('30%'),justifyContent:'space-around'

   },



  countryImage: {
    height: wp('10%'),
    width: wp('10%'),
    borderRadius:wp('10%/2'),
    marginRight:wp('5%')
  },
});
export default styles;
