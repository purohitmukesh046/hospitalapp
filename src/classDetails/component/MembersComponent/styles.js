import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  CardView: {
    height: wp('47%'),
    width: wp('90%'),
    backgroundColor: Colors.white_color,
    shadowColor: '#000',
    
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.20,
    shadowRadius: 3,
    alignSelf: 'center',
    elevation: 5,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#fff',
    marginTop :wp('5%')
    
  },
  ViewLine:{
  height:1,
  backgroundColor:Colors.greyButton_color,
  marginTop:7

  },

 
  otherIcon: {
    height: wp('5%'),
    width: wp('5%'),
  },

  addMemverIcon:{
    marginLeft:wp('2%'),
    height:30,
    width:30,
  },
  MembersText:{
    marginLeft :10,
    fontWeight :'700',
    fontSize:wp('4%'),
    alignSelf : 'center',
    color:Colors.greyButton_color
  },
  userProfile: {
    height: wp('15%'),
    width: wp('15%'),
    alignSelf:'flex-start',
   borderRadius:wp('20%/2'),
   marginTop:wp('3%')
    
  },

 inviteIcon:{

  height: wp('15%'),
  width: wp('15%'),
  alignSelf:'center',
 borderRadius:wp('20%/2'),
 },

listViewstyle:{
 
 alignSelf:'center',
 padding:wp('1%')
 

},


  MemberView:{
    padding:wp('5%'),
    marginTop:wp('5%'),
    flexDirection : 'row',
    justifyContent:'space-between',
    backgroundColor: Colors.white_color,
    borderRadius: 5,
    padding: 10,
    shadowColor: Colors.shadowColor,
    shadowOffset: {
      width: 0,
      height: 3
    },
    shadowRadius: 5,
    shadowOpacity: 1.0

  },

  userDetailText:{
    padding:wp('1.5%'),
    color:Colors.greyButton_color
    
 
   },
   userNameText:{
   fontWeight:'600',
    fontSize:wp('5%'),
    color:Colors.greyButton_color
   },
   hospitalNameText:{

    color:Colors.greyButton_color,
    marginBottom:4
   },

   dropDownView:
   {
    flexDirection : 'row',width:wp('30%'),justifyContent:'space-around'

   },



  countryImage: {
    height: wp('10%'),
    width: wp('10%'),
    borderRadius:wp('10%/2'),
    marginRight:wp('5%')
  },
});
export default styles;
