import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  View,Image
} from 'react-native'

import styles from '../StudentInfo/style'

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
function StudentInfo (props){
  
  console.warn("props are",props)
  return (
    <View style={styles.mainView}>
     <TouchableOpacity onPress={()=>props.currentProps.navigation.navigate('userProfile')}>

     <Image source={require('../../../../assests/Images/userProfile.png')} style= {styles.userProfile} />
     </TouchableOpacity>

     <View style={styles.studentIdView}> 
     <Text style={styles.studentName}> Emlen Beaver</Text>
     <Image source={require('../../../../assests/Images/editIcon.png')} style= {styles.editIcon} />

     </View>

     <View style={styles.studentIdView}> 
         <Text style={styles.IdText}>  Class ID :</Text>
         <Text style={styles.IdText}>53453453453</Text>
     </View>
   </View>
    )};
  export default StudentInfo 