import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions, ColorPropType} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  
    mainView:{

        flex :1,
        justifyContent:'center',
     
    },
    userProfile: {
        height: wp('20%'),
        width: wp('20%'),
        alignSelf:'center',
        borderRadius:wp('20%/2'),
        
      },

      editIcon:{

        height: wp('8%'),
        width: wp('8%'),
        alignSelf:'center',
        alignItems : 'center'
      },

     studentName:{
      padding:wp('5%'),
      color:Colors.white_color,
      fontSize:wp('5%'),
      fontWeight:'800',
      alignSelf:'center'

     },
     IdText:{
        fontSize:wp('3.5%'),
        color:Colors.white_color,
    fontWeight:'600'
     },
     studentIdView:{

        flexDirection : 'row',justifyContent:'center'

     },

});
export default styles;
