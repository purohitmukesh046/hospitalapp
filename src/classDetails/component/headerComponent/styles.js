import React from 'react';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {StyleSheet, Dimensions, ColorPropType} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from "../../../../utils/Colors";
const window = Dimensions.get('window');

const styles = StyleSheet.create({
  
    mainView:{
        marginTop:wp('5%'),
        height:wp('10%'),
       
    },

    headerTitileDetail:{
    fontSize:wp('6%'),
    fontWeight:'900',
    alignSelf : 'center',
    color : Colors.white_color
    }
  
});
export default styles;
