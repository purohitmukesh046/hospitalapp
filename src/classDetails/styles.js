
import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
import Colors from '../../utils/Colors'
const window = Dimensions.get('window');


const styles = StyleSheet.create({
    MainContainer: {
      flex: 1,
  
    },
   
    basicInfoText:{
      padding:wp('5%'),
      fontWeight:'700',
      fontSize:wp('5%')
  
   },
   bookStyle:{
    height: wp('40%'),
    width: wp('30%')
 
   },

   OuterView:{
    flexDirection: 'row', justifyContent: 'space-between'
   },
   

   bookName:{
    fontSize : wp('5%'),
    fontWeight:"800",
    marginLeft : wp('2%')

   },
   remainingBookView:{

    flexDirection:'row',justifyContent:'flex-start',marginTop:wp('10%'),marginLeft:wp('2%')
    
   },
  borrowText:{

    color:Colors.black_color,fontWeight:"800",alignSelf:'center'
  },
   borrowView:{
    flexDirection:'row',marginLeft:wp('4%'),backgroundColor:Colors.borrowBtnColor,
    shadowColor: Colors.shadowColor,
      height:wp('8%'),
      width:wp('20%'),
      
      shadowOffset: {
        width: 0,
        height: 2,
      },
      justifyContent:'center',
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      alignSelf: 'center',
      elevation: 5,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: Colors.borderColor,
      

   },
   remaainingSubView:
   {
    flexDirection:'row'
   },
   remainingText:{
    color:Colors.remainingTextColor,
   },

   remainingnumber:{
    
   color:Colors.redColor,
   fontWeight:'700'
   },
   bookDescription:{
 
    marginTop:wp('2%'),
    marginLeft : wp('3%'),
    color:Colors.remainingTextColor,
    width:'70%'
   },
    CardView: {
      height: wp('45%'),
      width: wp('90%'),
     backgroundColor:'white',
      shadowColor: Colors.shadowColor,
      
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      alignSelf: 'center',
      elevation: 5,
      borderRadius: 10,
      borderWidth: 1,
      borderColor: '#fff',
      marginTop :wp('7%'),
      padding:5,
     
      
    },

   seeAllText:{
     color : Colors.remainingTextColor

   },
   RecommendView:{
     padding:wp('5%'),
     marginTop:wp('5%'),
     flexDirection : 'row',
     justifyContent:'space-between',
     backgroundColor: Colors.white_color,
     borderRadius: 3,
     padding: 10,
     shadowColor: Colors.shadowColor,
     shadowOffset: {
       width: 0,
       height: 2
     },
     shadowRadius: 3,
     shadowOpacity: 1.0

   },

   RecommendText:{
    fontSize: wp('5%'),
    fontWeight:'800'

   },

    backgroundImage: {
      flex:1,
      width: wp('100%'),
      height: hp('36%'),
      position:'absolute'
      }
    });
 export default styles;