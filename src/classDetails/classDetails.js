import {
  StyleSheet,
  TouchableOpacity,
  Image,
  Text,
  KeyboardAvoidingView,
  View,
  ScrollView,
  FlatList,
  SafeAreaView,
} from 'react-native';
import React, {Component} from 'react';
import String from '../../utils/Strings'
import styles from '../classDetails/styles';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
// import { Header } from 'react-native/Libraries/NewAppScreen';
import HeaderComponent from '../classDetails/component/headerComponent';
import StudentInfo from '../classDetails/component/StudentInfo/index';
import MembersComponent from '../classDetails/component/MembersComponent/index';
export default class classDetails extends Component {
  constructor() {
    super();

    data = [
      {
        bookName: 'Android',
        bookDescription: 'best android book for beginner',
        remaining: '251',
      },
      {
        bookName: 'ios',
        bookDescription: 'best ios book for beginner',
        remaining: '251',
      },
      {
        bookName: 'react Native',
        bookDescription: 'best android book for beginner',
        remaining: '251',
      },
      {
        bookName: 'swift',
        bookDescription: 'best android book for beginner',
        remaining: '251',
      },
      {
        bookName: 'Android',
        bookDescription: 'best android book for beginner',
        remaining: '251',
      },
    ],
      MemberData = [
        {memberName: "require('../../../../assests/Images/userProfile.png')"},
        {memberName: "require('../../../../assests/Images/userProfile.png')"},
        {memberName: "require('../../../../assests/Images/userProfile.png')"},
      ];
  }

  //*************** book flatList data **************
  _renderBookList = () => {
    return (
      <FlatList
        style={{width: wp('90%')}}
        data={data}
        renderItem={this._renderData}
        horizontal={false}
        keyExtractor={(item, index) => index}
      />
    );
  };

  _renderData = ({item, index}) => (
    <>
      <View style={styles.CardView}>
        <View style={styles.OuterView}>
          <Image
            source={require('../../assests/Images/book.jpg')}
            style={styles.bookStyle}
          />
          <View>
            <Text style={styles.bookName}> {item.bookName}</Text>
            <Text style={styles.bookDescription}>{item.bookDescription}</Text>

            <View style={styles.remainingBookView}>
              <View style={styles.remaainingSubView}>
                <Text style={styles.remainingnumber}> {item.remaining}</Text>
                <Text style={styles.remainingText}> {String.remaining} </Text>
              </View>

              <View style={styles.borrowView}>
                  <Text style={styles.borrowText}>{String.borrow}</Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </>
  );

  //*******************end list */

  render() {
    // const{MemberData}=this.state
    return (
      <SafeAreaView style={{flex: 1}}>
         <Image
              source={require('../../assests/Images/backgroundImage.jpg')}
              style={styles.backgroundImage}
            />
                <HeaderComponent />
        <ScrollView>
          <View style={styles.MainContainer}>
           
        
            <StudentInfo currentProps={this.props} />
            <MembersComponent data={MemberData} />

            <View style={styles.RecommendView}>
              <View>
                {/* <Image source={require('../../assests/Images/menu.png')} /> */}

    <Text style={styles.RecommendText}>{String.recommend}</Text>
              </View>

              <View>
                <Text style={styles.seeAllText}>{String.seeAll}></Text>
                {/* <Image source={require('../../assests/Images/menu.png')} /> */}
              </View>
            </View>

            {/* flatelist Items */}
            <View style={{alignContent: 'center', alignSelf: 'center'}}>
              {this._renderBookList()}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
