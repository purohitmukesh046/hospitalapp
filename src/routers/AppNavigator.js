import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import classDetails from '../classDetails/classDetails'
import userProfile from '../userProfile'


const Stack = createStackNavigator();

const Navigator = () => {
    return (
        <NavigationContainer>

            <Stack.Navigator initialRouteName="ClassDetail" headerMode="none" >
                
                <Stack.Screen name="ClassDetail" 
                 component={classDetails}
                 options={{
                    headerTransparent: true,
                    headerMode: 'screen',
                  }}
                 />
                <Stack.Screen name="userProfile" component={userProfile}/>
            </Stack.Navigator>

        </NavigationContainer>
    )
};

export default Navigator
