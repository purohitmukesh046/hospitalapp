// import React, { Component } from 'react'
import {
  StyleSheet,
  TouchableOpacity,Image,
  Text,KeyboardAvoidingView,Animated,
  View,ScrollView,SafeAreaView,Alert,
} from 'react-native'
import React, {Component} from 'react';
import ParallaxScrollView from 'react-native-parallax-scroll-view';

import styles from './styles'
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

import UserData from '../src/profileComponent/userData/UserData.js'
import BasicInformation from '../src/profileComponent/basicInfo/BasicInformation.js'
import UserCase from '../src/profileComponent/case/UserCase.js'
import Strings from '../utils/Strings'
import Colors from '../utils/Colors'
export default class userProfile  extends Component {
  constructor(){
    super();
    this.state={
     
      isAddClicked:false,
      relativesCount:0,
      RelativesData :[
        {memberName: "require('../assests/Images/userProfile.png')"},
        {memberName: "require('../assests/Images/userProfile.png')"},
        {memberName: "require('../assests/Images/userProfile.png')"},
      ],
      
    }
    // RelativesData = [
    //   {memberName: "require('../assests/Images/userProfile.png')"},
    //   {memberName: "require('../assests/Images/userProfile.png')"},
    //   {memberName: "require('../assests/Images/userProfile.png')"},
    // ];
  }
 
  handleClick=()=>
  {
    const{isAddClicked,RelativesData,relativesCount}=this.state
    if(!isAddClicked)
    {
    let userData={

      memberName: "require('../assests/Images/userProfile.png')"
    }
    RelativesData.push(userData)
    this.setState({isAddClicked:true,relativesCount:RelativesData.length})
   }
  else
  {
   alert("user already added")
   console.warn("-------------------CLicked-----------------------")
  }
  }

  render() {
    const {RelativesData,isAddClicked,relativesCount}=this.state;
    let count =RelativesData.length
    return (
    
      <SafeAreaView style={{flex: 1}}>
         <Image source={require('../assests/Images/backgroundImage.jpg')} style= {styles.backgroundImage} />
      
         <UserData/>
        
     <ScrollView>
   
     
       <BasicInformation  data={RelativesData} userCount={count} onClick={this.handleClick}/>
    <Text  style={styles.basicInfoText}>{Strings.case}</Text>
       <View style={{flexDirection : 'row',justifyContent:'space-evenly'}}>
       <UserCase  viewColor={Colors.caseFirstBackground} textData={4} caseText={"Open"}/>
       <UserCase viewColor={Colors.caseSecondBackGround} textData={4} caseText={"Decided"}/>
       <UserCase  viewColor={Colors.caseThirdBackground} textData={4} caseText={"Related"}/>
       </View>
    <Text  style={styles.basicInfoText}>{Strings.document}</Text>
      
      </ScrollView>
      </SafeAreaView>
    );
  }

  


  // render() {
  //   return (
  //     <ParallaxScrollView
  //       backgroundColor="blue"
  //       contentBackgroundColor="pink"
  //       parallaxHeaderHeight={300}
  //       renderForeground={() => (
  //        <View style={{ flex: 1, backgroundColor:'green', alignItems: 'center', justifyContent: 'center' }}>
  //           <Text>Hello World!</Text>
  //         </View>
  //       )}>
  //       <View style={{ height: 500 }}>
  //         <Text>Scroll me</Text>
  //       </View>
  //     </ParallaxScrollView>
  //   );
  // }

}