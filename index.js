/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import userProfile from './src/userProfile'
import classDetails from './src/classDetails/classDetails.js'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
