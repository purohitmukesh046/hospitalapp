import {Text} from "react-native";
import React from "react";

const STRINGS = {

    //==============================================
    //================class details===============
  

    classDetail: 'Class Detail',
    member: 'Members',
    recommend:'Recommend',
    seeAll:"see all",
    classId:'Class ID',
    remaining:"Remaining",
   borrow:"Borrow",
   //==============================================
    //================user details===============


    basicInformation:"Basic Information",
    profile:"Profile",
    completionStatus:"Completion Status",
    modifiedOn:"Modified on",
    relative:"Relatives",
    totalRelative:"Total Relatives added",
    addRelatives:"+ Add Relatives",
    case:"Cases",
    document:"Document"





};
export default STRINGS