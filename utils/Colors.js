import {Text} from "react-native";
import React from "react";
const Colors={
    app_theme_color: '#4582E5',
    backGround_color:"#E5E5E5",
    white_color:"#ffffff",
    btn_color:"#FFC20F",
    black_color:"#000000",
    light_grey:"#E2E2E2",
    light_grey1:"#E9E9E9",
    placeholder_color:"#828282",
    policy_bgcolor:"#313131",
    floating_color:"#424242",
    earnedContainer_color: "#2D2D2D",
    borrowBtnColor:'#FFFF99',
    greyButton_color:'#5A5A5A',
    light_grey3:'#808080',
    failure_Toast: '#EA4242',
    step_indicator_circle: '#7DB508',
    step_indicator_unfinished_circle: "#acacac",
    membershipBackground: '#E4E4E4',
    transparent: 'transparent',
    shadowColor:'#000',
    borderColor:'#fff',
    remainingTextColor:'#696969',
    redColor:'#696969',
    addRelativeBtnColor: '#0000FF',
    // transparent: '#D9FFFFFF',
    customerType:'#CB3B3B',
    headerBottomLine:"#F1EFEF",
    tabBarActiveColor: '#1E68E0',
    textColor: '#686666',
    caseFirstBackground:'#5C62F9',
    caseSecondBackGround : '#4CD0FC',
    caseThirdBackground : '#FCD239'

}
export default Colors