import styled from "styled-components";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import ShadowView from "react-native-simple-shadow-view";
import {COLORS} from "../themes/Colors";
import {SafeAreaView} from 'react-navigation'

const MainContainer = styled.View`
flex: 1;
backgroundColor: ${COLORS.WHITE_COLOR};
alignItems: center;
`;

const ScrollContainer = styled.ScrollView`
flex: 1;
backgroundColor:${COLORS.WHITE_COLOR};
`;

const ShadowViewContainer = styled(ShadowView)`
 shadowColor: ${COLORS.SHADOW_COLOR};
 shadowOpacity: 1;
 shadowRadius: 3;
 borderRadius: 5;
 shadowOffset: 0px 2px;
 backgroundColor: ${COLORS.WHITE_COLOR};
 marginBottom: ${wp('4%')};
`;

const BorderViewContainer = styled.View`
 borderColor:${COLORS.GREY_1};
 borderWidth: 1 ;
 borderRadius:10;
 paddingVertical:4;
`;

const SafeAreaViewContainer = styled(SafeAreaView).attrs(() => ({
    forceInset: {top: 'never'}
}))`
flex:1;
`;

export {
    MainContainer,
    ScrollContainer,
    ShadowViewContainer,
    SafeAreaViewContainer,
    BorderViewContainer
}
